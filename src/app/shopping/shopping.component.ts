import { Component, OnInit } from '@angular/core';
import {ShoppingRepository} from './service/shopping-repository';

@Component({
  selector: 'app-shopping',
  templateUrl: './shopping.component.html',
  styleUrl: './shopping.component.css'
})
export class ShoppingComponent {

  public locations: any[];
  public products: any[];
  public shoppingList: any[];

  constructor(private shoppingRepository: ShoppingRepository) {
  }

  ngOnInit(): void {
    this.locations = this.shoppingRepository.getLocations();
    this.products = this.shoppingRepository.getProducts();
    this.shoppingList = this.shoppingRepository.getShoppingList();
  }

}
