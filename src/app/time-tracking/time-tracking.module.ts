import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {TimeTrackingRoutingModule} from './time-tracking-routing.module';
import {TimeTrackingComponent} from './time-tracking.component';
import {CommonModule} from '@angular/common';


@NgModule({
  declarations: [
    TimeTrackingComponent
  ],
  imports: [
    RouterModule,
    TimeTrackingRoutingModule,
    CommonModule
  ]
})
export class TimeTrackingModule {
}
