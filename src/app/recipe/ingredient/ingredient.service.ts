import {Injectable} from '@angular/core';
import {AppSettings} from '../../app.settings';
import { HttpClient } from '@angular/common/http';
import {Observable} from 'rxjs';
import {Ingredient} from '../model/ingredient';

@Injectable({
  providedIn: 'root'
})
export class IngredientService {
  private readonly INGREDIENT_URL = AppSettings.SERVER_URL + '/api/diet/ingredients';

  constructor(private http: HttpClient) {
  }

  // public getMovies(): Observable<IMovie[]> {
  //   return this.searchMovies('');
  // }

  // public searchMovies(queryParamString: string): Observable<IMovie[]> {
  //   return this.http.get<IMovie[]>(this.MOVIE_URL + queryParamString);
  // }
  //
  // public queryMovies(queryParam): Observable<IMovie[]> {
  //   return this.http.get<IMovie[]>(this.MOVIE_URL, { params: queryParam });
  // }

  public getIngredient(id: number): Observable<Ingredient> {
    return this.http.get<Ingredient>(this.INGREDIENT_URL + '/' + id);
  }

  // public createMovie(movie: IMovie): Observable<IMovie> {
  //   return this.http.post<IMovie>(this.MOVIE_URL, movie);
  // }
  //
  // public updateMovie(id: number, movie: IMovie): Observable<IMovie> {
  //   return this.http.put<IMovie>(this.MOVIE_URL + '/' + id, movie);
  // }
  //
  // public deleteMovie(id: number): Observable<{}> {
  //   return this.http.delete<any>(this.MOVIE_URL + '/' + id);
  // }
}
