import {Component, OnInit} from '@angular/core';

import {Ingredient} from '../../model/ingredient';
import {IngredientService} from '../ingredient.service';

@Component({
  selector: 'app-details',
  templateUrl: './ingredient-details.component.html',
  styleUrls: ['./ingredient-details.component.css']
})
export class IngredientDetailsComponent implements OnInit {
  ingredient: Ingredient;

  constructor(
    private ingredientService: IngredientService) {
  }

  ngOnInit(): void {
    this.retrieveIngredient(1);
  }

  deleteIngredient(ingredient: Ingredient): void {

  }

  private retrieveIngredient(id: number): void {
    this.ingredientService.getIngredient(id).subscribe(
      ingredient => this.ingredient = ingredient
    );
  }
}
