export interface Ingredient {
  id: number;
  name: string;
  nameGenitive: string;
  baseUnit: string;
  calories: number;
  carbs: number;
  fats: number;
  proteins: number;
  roughage: number;
  salt: number;
}
