import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {RecipeRoutingModule} from './recipe-routing.module';
import {RecipeComponent} from './recipe.component';
import {IngredientDetailsComponent} from './ingredient/details/ingredient-details.component';
import {CommonModule} from '@angular/common';


@NgModule({
  declarations: [
    RecipeComponent,
    IngredientDetailsComponent
  ],
  imports: [
    RouterModule,
    RecipeRoutingModule,
    CommonModule
  ]
})
export class RecipeModule {
}
