import {Component, OnInit} from '@angular/core';
import {ICryptoHolding} from '../model/ICryptoHolding';
import {CryptoService} from '../service/crypto-repository.service';

@Component({
  selector: 'app-crypto',
  templateUrl: './crypto.component.html',
  styleUrls: ['./crypto.component.css']
})
export class CryptoComponent implements OnInit {

  public cryptoHoldings: ICryptoHolding[] = [];
  public sumValue = 0;

  constructor(private cryptoService: CryptoService) {
  }

  public ngOnInit(): void {
    this.cryptoService.getCryptoHoldings().subscribe(
      cryptos => {
        this.cryptoHoldings = cryptos.assets;
        this.sumValue = cryptos.totalValue;
      }
    );
  }

}
