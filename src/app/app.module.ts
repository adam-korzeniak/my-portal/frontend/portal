import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import { HTTP_INTERCEPTORS, provideHttpClient, withInterceptorsFromDi } from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AuthModule} from './_legacy/auth/auth.module';
import {AuthInterceptor} from './_legacy/auth/interceptor/auth.interceptor';
import {CryptoModule} from './crypto/crypto.module';
import {FlashcardModule} from './flashcard/flashcard.module';
import {HomeModule} from './home/home.module';
import {TimeTrackingModule} from './time-tracking/time-tracking.module';
import {RecipeModule} from './recipe/recipe.module';
import {ErrorModule} from './_legacy/error/error.module';
import {MovieModule} from './_legacy/movies/movie.module';
import {ShoppingModule} from './shopping/shopping.module';

@NgModule({ declarations: [
        AppComponent
    ],
    bootstrap: [AppComponent], imports: [BrowserModule,
        FormsModule,
        ReactiveFormsModule,
        // TODO: Play with service workers
        // ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
        AuthModule,
        CryptoModule,
        FlashcardModule,
        ShoppingModule,
        HomeModule,
        RecipeModule,
        TimeTrackingModule,
        ErrorModule,
        MovieModule,
        AppRoutingModule], providers: [
        {
            provide: HTTP_INTERCEPTORS,
            useClass: AuthInterceptor,
            multi: true
        },
        provideHttpClient(withInterceptorsFromDi())
    ] })
export class AppModule {
}
