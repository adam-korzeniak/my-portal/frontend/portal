import {Injectable} from '@angular/core';
import { ActivatedRouteSnapshot, Router, RouterStateSnapshot } from '@angular/router';

import {AuthenticationService} from '../service/auth.service';

//TODO: USE THAT
@Injectable({providedIn: 'root'})
export class AuthGuard  {

  constructor(
    private router: Router,
    private authService: AuthenticationService) {
  }

  public canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    if (this.authService.isAuthorised()) {
      return true;
    }
    this.navigateToLoginPage(state);
    return false;
  }

  private navigateToLoginPage(state: RouterStateSnapshot): void {
    this.router.navigate(['/login'], {
      queryParams: {returnUrl: state.url.split('?')[0]}
    });
  }
}
